from uuid import uuid4, UUID
from typing import Union, Dict, Iterable

from serial import Serial, serialutil
from serial.threaded import ReaderThread, LineReader
from time import sleep

from . import coordinate_system as coord

STM_SERIAL_PORT = "/dev/ttyACM0"
BAUDRATE = 115200

Command = Dict[str, Union[str, str, Iterable[str]]]


class SerialReaderProtocolLine(LineReader):
    TERMINATOR = b'\n'

    def handle_line(self, line):
        """New line waiting to be processed"""
        StmManager.stm_manager.recieve(line)


class StmManager:
    def __init__(self) -> None:
        StmManager.stm_manager = self

        self._callback_for_uuid = {}
        self._recieved_commands = {}
        self._sent_commands = {}

        # begin UART connection...
        try:
            self._serial = Serial(port=STM_SERIAL_PORT, baudrate=BAUDRATE)
            self._operating = True
        except serialutil.SerialException:
            self._operating = False
            return

        # start recieving thread
        self._reader = ReaderThread(self._serial, SerialReaderProtocolLine)
        self._reader.start()

    @property
    def operating(self):
        return self._operating

    def recieve(self, command_line: str) -> None:
        tmp_list = command_line.split(";")
        command = {
            "command": tmp_list[0],
            "uuid": tmp_list[1],
            "args": tmp_list[2:],
        }
        if command["uuid"] in self._callback_for_uuid.keys():
            self._callback_for_uuid[command["uuid"]](command["args"])
        
        self._recieved_commands[command["uuid"]] = command # we can have both if the command is goto

    def has_been_recieved(self, uuid: str) -> bool:
        return uuid in self._recieved_commands.keys()

    def get_response(self, uuid: str, timeout : int = 1000) -> dict:
        """
        Blocking until a response is recieved
        """
        nb_waits = 0
        while not self.has_been_recieved(uuid):
            sleep(0.01)
            nb_waits += 1
            if(nb_waits*10 >= timeout):
                nb_waits = 0
                self._reader.write(self._sent_commands[uuid].encode("utf-8"))

        return self._recieved_commands[uuid]

    def _execute(self, stm_command: str, *args, callback: callable = None) -> str:
        if not self._operating:
            raise ConnectionError("You cannot send request, the connection failed.")

        uuid = str(uuid4())[:8]
        if callback is not None:
            self._callback_for_uuid[uuid] = callback
        # transmit command
        to_send = f"{stm_command};{uuid}" + "".join([f";{arg}" for arg in args]) + "\n"
        self._sent_commands[uuid] = to_send
        self._reader.write(to_send.encode("utf-8"))
        # self._reader.write(f"{stm_command};{uuid}" + "".join([f";{arg}" for arg in args]))
        return uuid

    def set_position(self, position: coord.Position) -> str:
        return self._execute("C", position.x, position.y, position.teta)
    
    def get_position(self, callback: callable = None) -> str:
        return self._execute("D", callback=callback)

    def go_to(self, position: coord.Position, callback: callable = None) -> str:
        return self._execute("P", position.x, position.y, position.teta, callback=callback)

    def set_absolute_speed(self, speed: coord.Speed) -> str:
        return self._execute("V", speed.x, speed.y, speed.teta)

    def set_relative_speed(self, speed: coord.Speed) -> str:
        return self._execute("VL", speed.x, speed.y, speed.teta)
    
    def set_obstacles(self, nb_obstacles:int = 0, obstacles:list[tuple[float]] = []) -> str:
        final_args = []
        for obs in obstacles:
            final_args.append(obs[0])
            final_args.append(obs[1])
            
        return self._execute("O", str(nb_obstacles), *final_args)
    
    def set_dimensions(self, wheel_radius : float, robot_radius : float) -> str:
        return self._execute("A", str(wheel_radius), str(robot_radius))
    
    def set_grippers(self, angles : tuple[int]) -> str:
        return self._execute("G", str(angles[0]), str(angles[1]), str(angles[2]), str(angles[3]))
    
    def set_ESC(self, state : bool) -> str:
        return self._execute("E", "1" if state else "0")
    
    def set_Trapdoor(self, state : bool) -> str:
        return self._execute("T", "1" if state else "0")
    
    def set_Disguise(self) -> str:
        return self._execute("R", "40")

    def stop(self) -> str:
        return self._execute("S")
